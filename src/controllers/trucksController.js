const express = require('express');
const router = express.Router();

const { getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById
} = require('../services/trucksService')
const { getActiveLoadByDriverId } = require('../services/loadsService')

const { asyncWrapepr } = require('../utils/apiUtils')
const { InvalidRequestError } = require('../utils/errors')
const { schema } = require('../services/validationService')

router.get('/', asyncWrapepr(async (req, res) => {
  const { userId } = req.user

  try {
    const trucks = await getTrucks(userId)
    res.status(200).json({ trucks })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.post('/', asyncWrapepr(async (req, res) => {
  const { userId } = req.user

  try {
    await addTruck(userId, req.body)
    res.status(200).json({ message: 'Truck created successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.get('/:id', asyncWrapepr(async (req, res) => {
  const { userId } = req.user
  const { id } = req.params

  try {
    const truck = await getTruckById(userId, id)
    if (!truck) {
      throw new InvalidRequestError
    }
    res.status(200).json({ truck })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.put('/:id', asyncWrapepr(async (req, res) => {
  const { userId } = req.user
  const { id } = req.params
  const { type } = req.body

  const load = await getActiveLoadByDriverId(userId)
  if(load) {
    throw new InvalidRequestError(`You can't edit trucks while assigned on load`)
  }

  if (!type) {
    throw new InvalidRequestError
  }

  const { error, value } = await schema.validate({ type: type })
  if (error) {
    throw new InvalidRequestError('Please, provide valid type')
  }

  const truck = await getTruckById(userId, id) 
  if (!truck) {
    throw new InvalidRequestError
  }

  if(truck.assigned_to !== null) {
    throw new InvalidRequestError(`You can't edit assigned truck`)
  }

  try {
    await updateTruckById(userId, id, type)
    res.status(200).json({ message: 'Truck details changed successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.delete('/:id', asyncWrapepr(async (req, res) => {
  const { userId } = req.user
  const { id } = req.params

  const load = await getActiveLoadByDriverId(userId)
  if(load) {
    throw new InvalidRequestError(`You can't delete trucks while assigned on load`)
  }

  const truck = await getTruckById(userId, id) 
  if (!truck) {
    throw new InvalidRequestError
  }

  if(truck.assigned_to !== null) {
    throw new InvalidRequestError(`You can't delete assigned truck`)
  }

  try {
    await deleteTruckById(userId, id)
    res.status(200).json({ message: 'Truck deleted successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.post('/:id/assign', asyncWrapepr(async (req, res) => {
  const { userId } = req.user
  const { id } = req.params

  const load = await getActiveLoadByDriverId(userId)
  if(load) {
    throw new InvalidRequestError(`You can't assign on truck while assigned on load`)
  }

  try {
    const truck = await assignTruckById(userId, id)
    if (!truck) {
      throw new InvalidRequestError
    }
    res.status(200).json({ message: 'Truck assigned successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

module.exports = {
  trucksRouter: router
}