const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt')

const { getProfile,
  deleteProfile,
  changePassword } = require('../services/usersService')
const { getActiveLoadByDriverId } = require('../services/loadsService')
const { asyncWrapepr } = require('../utils/apiUtils')
const { InvalidRequestError } = require('../utils/errors')

router.get('/', asyncWrapepr(async (req, res) => {
  const { email } = req.user

  try {
    const user = await getProfile(email)
    res.status(200).json({
      user: {
        _id: user._id,
        role: user.role,
        email: user.email,
        created_date: user.created_date
      }
    })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.delete('/', asyncWrapepr(async (req, res) => {
  const { email, userId } = req.user

  const load = await getActiveLoadByDriverId(userId)
  if(load) {
    throw new InvalidRequestError(`You can't delete profile assigned on load`)
  }
  try {
    await deleteProfile(email)
    res.status(200).json({ message: "Profile deleted successfully" })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.patch('/', asyncWrapepr(async (req, res) => {
  const { email, userId } = req.user

  const { newPassword, oldPassword } = req.body

  const load = await getActiveLoadByDriverId(userId)  
  if(load) {
    throw new InvalidRequestError(`You can't edit profile assigned on load`)
  }

  if (newPassword === '') {
    throw new InvalidRequestError('Provide new password')
  }
  try {
    await changePassword(email, oldPassword, newPassword)
    res.status(200).json({ message: 'Password changed successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

module.exports = {
  usersRouter: router
}
