const express = require('express');
const router = express.Router();

const { addLoad,
  getLoadsForDriver,
  getLoadsForShipper,
  getActiveLoadByDriverId,
  setLoadStateByDriverId,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadAndSearchForDriver,
  getActiveLoadById,
  getTruckByLoadId
} = require('../services/loadsService')

const { getTruckById
} = require('../services/trucksService')

const { asyncWrapepr } = require('../utils/apiUtils')
const { InvalidRequestError } = require('../utils/errors')
const { schema } = require('../services/validationService')

router.get('/', asyncWrapepr(async (req, res) => {
  const { userId, role } = req.user

  let status = req.query.status === undefined ? ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'] : req.query.status
  let limit = req.query.limit === undefined ? 10 : req.query.limit
  let offset = req.query.offset === undefined ? 0 : req.query.offset

  if (req.query.status) {
    const { error, value } = await schema.validate({ status: status })
    if (error) {
      throw new InvalidRequestError('Please, provide valid status')
    }
  }

  if (limit > 50) {
    limit = 50
  }

  if (offset > 50) {
    offset = 50
  }

  if (role === 'DRIVER') {
    if (status === 'NEW' || status === 'POSTED') {
      throw new InvalidRequestError('Please, provide valid status')
    }
    if (!req.query.status) {
      status = ['ASSIGNED', 'SHIPPED']
    }
    try {
      const loads = await getLoadsForDriver(userId, status, offset, limit)
      res.status(200).json({ loads })
    } catch (err) {
      throw new InvalidRequestError(err.message)
    }
  } else {
    try {
      const loads = await getLoadsForShipper(userId, status, offset, limit)
      res.status(200).json({ loads })
    } catch (err) {
      throw new InvalidRequestError(err.message)
    }
  }
}))

router.post('/', asyncWrapepr(async (req, res) => {
  const { userId, role } = req.user

  if (role !== 'SHIPPER') {
    throw new InvalidRequestError('Access denied')
  }

  try {
    await addLoad(userId, req.body)
    res.status(200).json({ message: 'Load created successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.get('/active', asyncWrapepr(async (req, res) => {
  const { userId, role } = req.user

  if (role !== 'DRIVER') {
    throw new InvalidRequestError('Access denied')
  }

  try {
    const load = await getActiveLoadByDriverId(userId)
    res.status(200).json({ load })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.patch('/active/state', asyncWrapepr(async (req, res) => {
  const { userId, role } = req.user

  if (role !== 'DRIVER') {
    throw new InvalidRequestError('Access denied')
  }

  try {
    const load = await getActiveLoadByDriverId(userId)
    if (!load) {
      throw new InvalidRequestError('No load found')
    } else {
      let state = load.state
      const states = [null, 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']
      let index = states.indexOf(state)
      if (index === states.length - 1) {
        throw new InvalidRequestError(`You can't change state '${state}'`)
      } else {
        state = states[index + 1]
      }
      await setLoadStateByDriverId(userId, state)
      res.status(200).json({ message: `Load state changed to '${state}'` })
    }
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.get('/:id', asyncWrapepr(async (req, res) => {
  const { userId } = req.user
  const { id } = req.params

  try {
    const load = await getLoadById(userId, id)
    if (!load) {
      throw new InvalidRequestError('No load found')
    }
    res.status(200).json({ load })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.put('/:id', asyncWrapepr(async (req, res) => {
  const { userId, role } = req.user
  const { id } = req.params

  if (role !== 'SHIPPER') {
    throw new InvalidRequestError('Access denied')
  }

  if (Object.keys(req.body).length === 0) {
    throw new InvalidRequestError('Please provide body parameters')
  }

  try {
    const load = await updateLoadById(userId, id, req.body)
    if (!load) {
      throw new InvalidRequestError('No load found')
    }
    res.status(200).json({ message: 'Load details changed successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.delete('/:id', asyncWrapepr(async (req, res) => {
  const { userId, role } = req.user
  const { id } = req.params

  if (role !== 'SHIPPER') {
    throw new InvalidRequestError('Access denied')
  }

  try {
    const load = await deleteLoadById(userId, id)
    if (!load) {
      throw new InvalidRequestError('No load found')
    }
    res.status(200).json({ message: 'Load deleted successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.post('/:id/post', asyncWrapepr(async (req, res) => {
  const { userId, role } = req.user
  const { id } = req.params

  if (role !== 'SHIPPER') {
    throw new InvalidRequestError('Access denied')
  }

  try {
    const driver_found = await postLoadAndSearchForDriver(userId, id)
    if (driver_found) {
      res.status(200).json({ message: 'Load posted successfully', driver_found: driver_found })
    } else {
      res.status(200).json({ message: 'Load was not posted', driver_found: driver_found })
    }
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.get('/:id/shipping_info', asyncWrapepr(async (req, res) => {
  const { userId, role } = req.user
  const { id } = req.params

  if (role !== 'SHIPPER') {
    throw new InvalidRequestError('Access denied')
  }

  try {
    const load = await getActiveLoadById(userId, id)
    if (!load) {
      res.status(400).json({ message: 'Load not found' })
    } else {
      const truck = await getTruckByLoadId(load.assigned_to)
      res.status(200).json({ load, truck })
    }
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

module.exports = {
  loadsRouter: router
}