const express = require('express');
const router = express.Router();

const { registration, login, forgotPassword } = require('../services/authService')
const { asyncWrapepr } = require('../utils/apiUtils')
const { schema } = require('../services/validationService')
const { InvalidRequestError } = require('../utils/errors')

router.post('/register', asyncWrapepr(async (req, res) => {
  const {
    email,
    password,
    role
  } = req.body

  if (!email) {
    throw new InvalidRequestError('Email is required')
  }
  if (!password) {
    throw new InvalidRequestError('Password is required')
  }
  if (!role) {
    throw new InvalidRequestError('Role is required')
  }

  const { error, value } = await schema.validate({ email: email })

  if (error) {
    throw new InvalidRequestError('Please, provide valid email')
  }

  try {
    await registration({ email, password, role })
    res.status(200).json({ message: 'Profile created successfully' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.post('/login', asyncWrapepr(async (req, res) => {
  const {
    email,
    password
  } = req.body

  try {
    const jwt_token = await login({ email, password })
    res.status(200).json({ jwt_token: jwt_token })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

router.post('/forgot_password', asyncWrapepr(async (req, res) => {
  const { email } = req.body

  try {
    await forgotPassword(email)
    res.status(200).json({ message: 'New password sent to your email address' })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}))

module.exports = {
  authRouter: router
}