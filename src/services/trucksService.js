const { Truck } = require('../models/truckModel')
const { InvalidRequestError } = require('../utils/errors')

const getTrucks = async (created_by) => {
  const trucks = await Truck.find({ created_by })  
  return trucks
}

const addTruck = async (created_by, truckPayload) => {
  const truck = new Truck({ ...truckPayload, created_by })
  await truck.save()
}

const getTruckById = async (created_by, truckId) => {
  const truck = await Truck.findOne({ _id: truckId, created_by })
  return truck
}

const updateTruckById = async (created_by, truckId, type) => {
  await Truck.findOneAndUpdate({ _id: truckId, created_by }, {type: type})
}

const deleteTruckById = async (created_by, truckId) => {
  await Truck.findByIdAndRemove({ _id: truckId, created_by })
}

const assignTruckById = async (created_by, truckId) => {
  await Truck.findOneAndUpdate({ assigned_to: created_by }, {assigned_to: null})
  const truck = await Truck.findOneAndUpdate({ _id: truckId, created_by }, {assigned_to: created_by})
  return truck
}

module.exports = {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById
}
