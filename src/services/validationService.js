const joi = require('joi')

const schema = joi.object({
  email: joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
  type: joi.string()
    .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'),
  status: joi.string()
    .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED')
})

module.exports = {
  schema
}