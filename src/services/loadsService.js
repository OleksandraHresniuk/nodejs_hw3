const { Load } = require('../models/loadModel')
const { Truck } = require('../models/truckModel')

const getLoadsForDriver = async (assigned_to, status, offset, limit) => {
  const loads = await Load.find({ assigned_to, status })
    .skip(+offset)
    .limit(+limit)
  return loads
}

const getLoadsForShipper = async (created_by, status, offset, limit) => {
  const loads = await Load.find({ created_by, status })
    .skip(+offset)
    .limit(+limit)
  return loads
}

const addLoad = async (created_by, loadPayload) => {
  const load = new Load({ ...loadPayload, created_by })
  await load.save()
}

const getActiveLoadByDriverId = async (assigned_to) => {
  const load = await Load.findOne({ assigned_to, status: 'ASSIGNED' })
  return load
}

const setLoadStateByDriverId = async (assigned_to, state) => {
  if(state === 'Arrived to delivery') {
    await Load.findOneAndUpdate({ assigned_to, status: 'ASSIGNED' }, { state: state, status: 'SHIPPED', logs: { message: `Load state changed to '${state}'`, time: Date.now() } })
    await Truck.findOneAndUpdate({ assigned_to }, { status: 'IS' })
  } else {
    await Load.findOneAndUpdate({ assigned_to, status: 'ASSIGNED' }, { state: state, logs: { message: `Load state changed to '${state}'`, time: Date.now() } })
  }  
}

const getLoadById = async (created_by, id) => {
  const load = await Load.findOne({ _id: id, created_by })
  return load
}

const updateLoadById = async (created_by, id, loadPayload) => {
  const load = await Load.findOneAndUpdate({ _id: id, created_by, status: 'NEW' }, { ...loadPayload, logs: { message: `Load was updated`, time: Date.now() } })
  return load
}

const deleteLoadById = async (created_by, id) => {
  const load = await Load.findOneAndRemove({ _id: id, created_by, status: 'NEW' })
  return load
}

const postLoadAndSearchForDriver = async (created_by, id) => {
  const truckTypes = [
    {
      name: 'SPRINTER',
      payload: 1700,
      length: 300,
      height: 250,
      width: 170
    },
    {
      name: 'SMALL STRAIGHT',
      payload: 2500,
      length: 500,
      height: 250,
      width: 170
    },
    {
      name: 'LARGE STRAIGHT',
      payload: 4000,
      length: 700,
      height: 350,
      width: 200
    }]

  const load = await Load.findOneAndUpdate({ _id: id, created_by }, { status: 'POSTED' })

  const truckTypeObj = truckTypes.filter(truck => truck.payload > load.payload && truck.length > load.dimensions.length && truck.height > load.dimensions.height && truck.width > load.dimensions.width)
  const truckType = truckTypeObj.map(truck => { return truck.name })

  if (truckType.length === 0) {
    const driver_found = false
    return driver_found
  } else {
    const truck = await Truck.findOneAndUpdate({ assigned_to: { $ne: null }, status: 'IS', type: truckType }, { status: 'OL' })

    if (!truck) {
      await Load.findOneAndUpdate({ _id: id, created_by }, { status: 'NEW' })
      const driver_found = false
      return driver_found
    }

    await Load.findOneAndUpdate({ _id: id, created_by }, { status: 'ASSIGNED', state: 'En route to Pick Up', assigned_to: truck.assigned_to, logs: { message: `Load assigned to driver with id '${truck.assigned_to}'`, time: Date.now() } })
    const driver_found = true
    return driver_found
  }
}

const getActiveLoadById = async (created_by, id) => {
  const load = await Load.findOne({ _id: id, created_by, status: 'ASSIGNED' })
  return load
}

const getTruckByLoadId = async (assigned_to) => {
  const truck = await Truck.findOne({ assigned_to })
  return truck
}


module.exports = {
  addLoad,
  getLoadsForDriver,
  getLoadsForShipper,
  getActiveLoadByDriverId,
  setLoadStateByDriverId,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadAndSearchForDriver,
  getActiveLoadById,
  getTruckByLoadId
}
