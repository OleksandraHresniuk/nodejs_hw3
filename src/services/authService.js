const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const generator = require('generate-password')
require('dotenv').config()

const { User } = require('../models/userModel')

const registration = async ({email, password, role}) => {
  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role
  }) 
  await user.save()
}

const login = async ({email, password}) => {
  
  const user = await User.findOne({email})

  if (!user) {
    throw new Error('Invalid email or password')
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid email or password')
  }

  const secret = process.env.TOKEN_SECRET

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role
  }, secret)
  return token
}

const forgotPassword = async(email) => {
  const user = await User.findOne({email})

  if (!user) {
    throw new Error('No user found')
  }

  const newPassword = generator.generate({
    length: 10,
    numbers: true
  })
  console.log(newPassword);
  await User.findOneAndUpdate({email}, {password: await bcrypt.hash(newPassword, 10)})
}

module.exports = {
  registration,
  login,
  forgotPassword
}
