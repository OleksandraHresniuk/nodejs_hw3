const bcrypt = require('bcrypt')
const { User } = require('../models/userModel')

const getProfile = async (email) => {
  const user = await User.findOne({ email })
  return user
}

const deleteProfile = async (email) => {
  await User.findOneAndDelete({ email })
}

const changePassword = async (email, oldPassword, newPassword) => {
  const user = await User.findOne({ email })

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid password')
  }
  
  await User.findOneAndUpdate({email}, {password: await bcrypt.hash(newPassword, 10)})
}

module.exports = {
  getProfile,
  deleteProfile,
  changePassword
}
