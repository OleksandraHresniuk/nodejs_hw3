const { InvalidRequestError } = require('../utils/errors')

const driverMiddleware = (req, res, next) => {
  const { role } = req.user

  if (role !== 'DRIVER') {
    throw new InvalidRequestError('Access denied')
  } else {
    next()
  }
}

module.exports = {
  driverMiddleware
}
