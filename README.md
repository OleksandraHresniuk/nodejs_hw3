# BEST Delivery

This service was created to help regular people to deliver their stuff and help drivers to find loads
and earn money.

## Installation

Install [Node.js](https://nodejs.org/uk/) on your computer.
Open terminal in app root folder and install it:

```bash
npm install
```

Then run apllication with command:

```bash
npm start
```

[Read API documentation](src/api-docs/index.html)